package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        
        return currentGeneration.get(row,column);
    }

    @Override
    public void initializeCells() {
        // TODO Auto-generated method stub
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        // TODO Auto-generated method stub
        IGrid nextGeneration = currentGeneration.copy();

		for (int rows=0; rows<this.numberOfRows(); rows++){
			for (int columns=0; columns<this.numberOfColumns(); columns++){
				CellState nextCell = getNextCell(rows, columns);
				nextGeneration.set(rows, columns, nextCell);
			}		
		}
		currentGeneration=nextGeneration;
        
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO Auto-generated method stub
        CellState nextCell=CellState.DEAD;
		CellState currentState = getCellState(row, col);
		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
		
		if (currentState==CellState.ALIVE){
			nextCell=CellState.DYING;
		} else if (currentState==CellState.DYING){
			nextCell=CellState.DEAD;
		} else if (aliveNeighbors==2 && currentState==CellState.DEAD){
			nextCell=CellState.ALIVE;
		} else {
            nextCell=CellState.DEAD;

        
		}
		return nextCell;
        
    }

    @Override
    public int numberOfRows() {
        // TODO Auto-generated method stub
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO Auto-generated method stub
        return currentGeneration.numColumns();
    }


    private int countNeighbors(int row, int col, CellState state) {
	
		int counter=0;		
		for (int i=row-1; i<=row+1; i++){
			for (int n=col-1; n<col+2; n++){				
				if(i<0 || n<0 ){
					continue;
				}
				else if (i>=currentGeneration.numRows()||n>=currentGeneration.numColumns() ){
					continue;
				}
				else if (i==row && n==col){
					continue;
				}
				else if (getCellState(i,n).equals(state)){
										
					counter++;				
				}
			}
		}
		return counter;
	}

    @Override
    public IGrid getGrid() {
        // TODO Auto-generated method stub
        return currentGeneration;
    }
    
}
