package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;

    private int columns;

    private CellState initialState;

    private CellState [][] grid;
    
    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this. columns = columns;
        this. initialState = initialState;
        grid =new CellState [rows][columns];
        for (int i=0; i<rows; i++){
            for (int n=0; n<columns; n++){
                grid[i][n]=(initialState);
            }
        }        
	    }
    @Override
    public int numRows() {        
        return rows;
    }

    @Override
    public int numColumns() {        
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (0>row || row>rows || 0>column || column>columns){
            throw new IndexOutOfBoundsException();
        }  
        grid[row][column]=element;        
    }

    @Override
    public CellState get(int row, int column) {        
        if (0>row || row>rows || 0>column || column>columns){
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        
        IGrid newGrid = new CellGrid(numRows(), numColumns(), initialState);
        for (int i=0; i<rows; i++){
            for (int n=0; n<columns; n++){
                newGrid.set(i, n, grid[i][n]);                
            }
        }
        return newGrid;
    }
    
}
